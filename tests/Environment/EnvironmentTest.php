<?php

namespace Plantera\Tests\Environment;

use PHPUnit\Framework\TestCase;
use Plantera\Environment\Environment;
use Plantera\Environment\Exceptions\InvalidRootDirException;

class EnvironmentTest extends TestCase
{
    public function testEnvironmentCanGetDotEnvFileSuccessfully(): void
    {
        $this->expectException(InvalidRootDirException::class);

        $env = new Environment();
    }

    public function testEnvironmentCanGetValuesFromDotEnvFile(): void
    {
        $env = $this->environmentFactory();

        $appEnv = $env->get('APP_ENV');

        $this->assertIsString($appEnv);
    }

    public function testEnvironmentCanPutNewEnvVariableOnDotEnvFile(): void
    {
        $env = $this->environmentFactory();

        $env->set('NEW_VAR', 'a_new_var_value');

        $newVarValue = $env->get('NEW_VAR');

        $this->assertIsString($newVarValue);
    }

    private function environmentFactory(): Environment
    {
        return new Environment(__DIR__);
    }
}