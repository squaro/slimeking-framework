<?php

/**
 * -----------------------------------------------------------------------------
 * REGISTER THE COMPOSER AUTO LOAD
 * -----------------------------------------------------------------------------
 *
 * Composer provides a convenient, automatically generated class loader
 * for our application. We just need to utilize it! We'll require it
 * into the script here so that we do not have to worry about the
 * loading of any our classes "manually". Feels great to relax.
 */

require __DIR__.'/../vendor/autoload.php';

/**
 * -----------------------------------------------------------------------------
 * SET THE DEFAULT TIMEZONE
 * -----------------------------------------------------------------------------
 *
 * Here we will set the default timezone for PHP. Actually is setted on Buenos
 * Aires, Argentina.
 */

date_default_timezone_set('America/Argentina/Buenos_Aires');
