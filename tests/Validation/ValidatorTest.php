<?php

namespace Plantera\Tests\Validation;

use PHPUnit\Framework\TestCase;
use Plantera\Validation\Validator;
use Plantera\Validation\Exceptions\InvalidRuleException;
use Plantera\Validation\Exceptions\InvalidArgumentException;

class ValidatorTest extends TestCase {

    public function testCanCreateValidatorInstanceWithSuccess(): void
    {
        $data = [];
        $rules = [];
        $validator = new Validator($data, $rules);

        $this->assertInstanceOf(Validator::class, $validator);
    }


    public function testCreateValidatorWithNullParametersThrowArgumentException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $data = null;
        $rules = null;
        $validator = new Validator($data, $rules);

        $this->assertInstanceOf(Validator::class, $validator);
    }

    public function testCreateValidatorWithBadParametersTypeThrowArgumentException(): void
    {
        $this->expectException(InvalidArgumentException::class);

        $data = '';
        $rules = 45;
        $validator = new Validator($data, $rules);

        $this->assertInstanceOf(Validator::class, $validator);
    }

    public function testValidateDataWithCorrectRuleStructureDontThrowExceptions(): void
    {
        $data = [
            'name' => 'Foo',
            'lastname' => 'Bar',
            'age' => 22
        ];
        $rules = [
            'name' => 'required',
            'lastname' => 'required',
            'age' => 'required|numeric'
        ];

        $validator = new Validator($data, $rules);

        $this->assertTrue($validator->validate()->isValid());
    }

    public function testValidateDataWithIncorrectRuleStructureThrowException(): void
    {
        $this->expectException(InvalidRuleException::class);

        $data = [
            'name' => 'Foo'
        ];
        $rules = [
            'name' => 'invalid_method'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();
    }

    public function testValidateOnBadDataCanAddErrors(): void
    {
        $data = [
            'age' => 'NaN'
        ];
        $rules = [
            'age' => 'numeric'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();

        $this->assertCount(1, $validator->getErrors());
    }

    public function testValidateOnFailedDataCanAddMultipleErrors(): void
    {
        $data = [
            'age' => ''
        ];
        $rules = [
            'age' => 'required|numeric'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();

        $this->assertIsArray($validator->getErrors()['age']);
    }

    /**
     * Los siguiente test van a ser removidos cuando se abstraiga la clase de Rules
     */

    public function testValidateEmptyStringRequiredRuleAddError(): void
    {
        $data = [
            'name' => ''
        ];
        $rules = [
            'name' => 'required'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();

        $this->assertTrue(count($validator->getErrors()) != 0);
        $this->assertArrayHasKey('name', $validator->getErrors());
    }

    public function testValidateEmailRuleWithBadEmailAddError(): void
    {
        $data = [
            'email' => 'this_isnt_email'
        ];
        $rules = [
            'email' => 'email'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();

        $this->assertArrayHasKey('email', $validator->getErrors());
    }

    public function testValidateNumericFieldWithNotNumericDataAddError(): void
    {
        $data = [
            'age' => 'NaN'
        ];
        $rules = [
            'age' => 'numeric'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();

        $this->assertArrayHasKey('age', $validator->getErrors());
    }

    public function testValidateMaxLengthWithBadDataAddError(): void
    {
        $data = [
            'password' => 'password12345678_'
        ];
        $rules = [
            'password' => 'max_length=16'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();

        $this->assertArrayHasKey('password', $validator->getErrors());
    }

    public function testValidateMinLengthWithBadDataAddError(): void
    {
        $data = [
            'password' => 'psswd'
        ];
        $rules = [
            'password' => 'min_length=8'
        ];

        $validator = new Validator($data, $rules);

        $validator->validate();

        $this->assertArrayHasKey('password', $validator->getErrors());
    }

}
