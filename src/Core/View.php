<?php

namespace Plantera\Core;

use Plantera\Core\ClientPipe\Response;

/**
 * Class View
 * @package Plantera\Core
 *
 * Clase para la muestra de datos.
 */
class View
{
  /**
   * Devuelve el valor como JSON
   *
   * @param Response $response
   */
  protected static function renderJson($response)
  {
    header('Content-Type: application/json; charset=utf-8');
    echo json_encode($response);
    exit;
  }

  /**
   * Devuelve los errores.
   *
   * @param int $code
   * @param string|array|null $messages
   */
  public static function render($code, $messages = null)
  {
    self::renderJson(new Response($code, $messages));
  }
}
