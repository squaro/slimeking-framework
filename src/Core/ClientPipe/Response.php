<?php

namespace Plantera\Core\ClientPipe;

use Exception;
use JsonSerializable;
use Plantera\Core\View;

/**
 * Class Error
 * @package Plantera\Core\ClientPipe
 *
 * Objeto que se encarga del manejo de los errores para responder desde la
 * vista.
 */
class Response implements JsonSerializable
{
  /**
   * Estado de la respuesta
   *
   * @var boolean
   */
  protected $status;

  /**
   * Código del error
   *
   * @var int
   */
  protected $code;

  /**
   * Mensajes de respuesta
   *
   * @var string|array|null
   */
  protected $data = null;

  /**
   * Metodo mágico __get()
   *
   * @param string $key
   * @return mixed
   */
  public function __get($key)
  {
    return $this->{$key};
  }

  /**
   * Metodo mágico __set()
   *
   * @param string $key
   */
  public function __set($key, $value)
  {
    $this->{$key} = $value;
  }

  /**
   * Constructor de Errores
   *
   * @param int               $code
   * @param string|array|null $data
   */
  public function __construct($code, $data = null)
  {
    $this->code = $code;

    if(!is_null($data)) {
      $this->data = $data;
    }

    try {
      $this->status = $this->getStatusByCode($code);
    }
    catch(Exception $e) {
      View::render(5, 'No se pudo procesar el pedido.');
    }

  }

  public function jsonSerialize() {
    return [
      'success'  => $this->status,
      'code'    => $this->code,
      'data'    => $this->data,
    ];
  }

  /**
   * Obtiene el valor del estado por el código.
   *
   * @param int $code
   * @throws Exception
   * @return boolean
   */
  public function getStatusByCode($code)
  {
    /**
     * Status codes:
     * 0 => Client side error (Validación, Forms).
     * 1 => Success.
     * 2 => Network error (Conexión con la base de datos).
     * 3 => 404 Error (Bad request).
     * 4 => Auth required.
     * 5 => 500 Api error.
     */
    $falseStatusCodes = [0, 2, 3, 4, 5];
    $trueStatusCodes = [1];

    if(!is_int($code)) {
      throw new Exception('El formato del código de respuesta es incorrecto.');
    }

    if(in_array($code, $falseStatusCodes)) {
      return false;
    }
    else if(in_array($code, $trueStatusCodes)) {
      return true;
    }
    else {
      throw new Exception('El código de respuesta no es válido.');
    }
  }

  /**
   * Getter $status
   *
   * @return boolean
   */
  public function getStatus()
  {
    return $this->status;
  }

  /**
   * Setter $status
   *
   * @param boolean
   */
  public function setStatus($status)
  {
    $this->status = $status;
  }

  /**
   * Getter $code
   *
   * @return boolean
   */
  public function getCode()
  {
    return $this->code;
  }

  /**
   * Setter $code
   *
   * @param int
   */
  public function setCode($code)
  {
    $this->code = $code;
  }

  /**
   * Getter $data
   *
   * @return boolean
   */
  public function getData()
  {
    return $this->data;
  }

  /**
   * Setter $data
   *
   * @param boolean
   */
  public function setData($data)
  {
    $this->data = $data;
  }
}
