<?php

namespace Plantera\Core\ClientPipe;

use Plantera\Core\App;

/**
 * Class Request
 * @package Plantera\Core\ClientPipe
 *
 * Se encarga de obtener la ruta y la accion de la request.
 */
class Request
{
  /**
   * Url requerida
   *
   * @var string
   */
  protected $requestUrl;

  /**
   * Verbo HTTP con el que se ejecuta la request
   *
   * @var string
   */
  protected $requestAction;

  /**
   * Metodo constructor
   */
  public function __construct()
  {
    $this->requestAction = $_SERVER['REQUEST_METHOD'];

    $absoluteUrl = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'];

    $this->requestUrl = str_replace(App::getPublicPath(), '', $absoluteUrl);
  }

  /**
   * Devuelve el valor de $requestUrl
   *
   * @return string
   */
  public function getRequestUrl()
  {
    return $this->requestUrl;
  }

  /**
   * Devuelve el valor de $requestAction
   *
   * @return string
   */
  public function getRequestAction()
  {
    return $this->requestAction;
  }

}
