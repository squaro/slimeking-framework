<?php

namespace Plantera\Core;

/**
 * Class Router
 * @package Plantera\Core
 *
 * Se encarga de el manejo de las urls de la aplicación.
 */
class Router
{
  /**
   * Variable donde se van a almacenar todas la rutas.
   *
   * @var array
   */
  protected static $routes = [
    'GET'     => [],
    'POST'    => [],
    'PUT'     => [],
    'PATCH'   => [],
    'DELETE'  => [],
  ];

  /**
   * Guarda los parametros de la url si es una url parametrizada.
   *
   * @var array
   */
  protected static $urlParameters = [];

  /**
   * Configuración del controller
   *
   * @var array
   */
  protected static $controllerAction = null;

  /**
   * Agrega una ruta a la App.
   *
   * @param string $url
   * @param array $settings
   */
  public static function add($url, $settings)
  {
    $method = $settings['method'] ?? 'GET';

    self::$routes[strtoupper($method)][$url] = [
      'controller' => $settings['controller'],
      'action' => $settings['action'],
    ];
  }

  /**
   * Verifica si la url que recibe está registrada.
   *
   * @param string $actionMethod
   * @param string $requestUrl
   * @return boolean
   */
  public static function exists($actionMethod, $requestUrl)
  {
    if(isset(self::$routes[$actionMethod][$requestUrl])) {
      return true;
    }
    else if(self::parametrizedRouteExists($actionMethod, $requestUrl)) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Verifica si la ruta requerida esta paremetrizada.
   *
   * @param string $actionMethod
   * @param string $requestUrl
   * @return boolean
   */
  public static function parametrizedRouteExists($actionMethod, $requestUrl)
  {
    $urlExplode = explode('/', $requestUrl);

    foreach(self::$routes[$actionMethod] as $route => $controllerAction) {

      $routeParts = explode('/', $route);
      $routeMatches = true;
      $urlData = [];

      if(count($routeParts) != count($urlExplode)) {
        $routeMatches = false;
      }
      else {

        foreach($routeParts as $key => $part) {

          if($routeParts[$key] != $urlExplode[$key]) {

            if(strpos($routeParts[$key], '{') === 0) {

              $paramName = substr($routeParts[$key], 1, -1);
              $urlData[$paramName] = $urlExplode[$key];

            }
            else {
              $routeMatches = false;
            }

          }

        }

      }

      if($routeMatches) {

        self::$controllerAction = $controllerAction;
        self::$urlParameters = $urlData;

        return true;

      }

    }

    return false;
  }

  /**
   * Obtiene los datos del controller
   *
   * @param string $actionMethod
   * @param string $requestUrl
   * @return array
   */
  public static function getController($actionMethod, $requestUrl)
  {
    if(!is_null(self::$controllerAction)) {
      return self::$controllerAction;
    }

    return self::$routes[$actionMethod][$requestUrl];
  }

  /**
   * Getter $urlParameters
   *
   * @return array
   */
  public static function getUrlParameters()
  {
    return self::$urlParameters;
  }


}
