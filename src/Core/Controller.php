<?php

namespace Plantera\Core;

use Exception;
use Plantera\Core\Router;
use Plantera\Core\View;

/**
 * Class Controller
 * @package Plantera\Core
 *
 * Clase generica que va a ser implementada en todos los controllers.
 */
class Controller
{
  /**
   * Guarda el modelo destinado al controller
   *
   * @var Model
   */
  protected $model;

  /**
   * Almacena los datos del buffer de PHP en el caso de que existan.
   *
   * @var array
   */
  protected $data;

  /**
   * Almacena los datos que vienen de la URL en el caso de que existan.
   *
   * @var array
   */
  protected $params;

  /**
   * Constructor
   *
   * @param string $action
   */
  public function __construct($action)
  {
    if(!method_exists($this, $action)) {
      throw new Exception('No se pudo ejecutar el método: ' . $action);
    }

    $this->getUrlParameters();
    $this->getBufferData();
  }

  /**
   * Metodo mágico __get()
   *
   * @param string $key
   * @return mixed
   */
  public function __get($key)
  {
    // echo $key;
    return $this->{$key};
  }

  /**
   * Metodo mágico __set()
   *
   * @param string $key
   */
  public function __set($key, $value)
  {
    $this->{$key} = $value;
  }

  /**
   * Instancia el modelo destinado al controller hijo.
   *
   * @param string $model
   */
  public function initModel($model)
  {
    $modelClass = '\\Plantera\\Models\\' . $model;
    $this->setModel(new $modelClass);
  }

  public function getUrlParameters()
  {
    $this->setParams(Router::getUrlParameters());
  }

  /**
   * Checkea si el buffer de PHP tiene contenido y lo asigna a $data
   */
  public function getBufferData()
  {
    $phpBuffer = file_get_contents('php://input');

    if($phpBuffer != '') {
      $this->setData(json_decode($phpBuffer, true));
    }
  }

  /**
   * Setter de $model
   *
   * @param Model $model
   */
  public function setModel($model)
  {
    $this->model = $model;
  }

  /**
   * Getter $model
   *
   * @return Model
   */
  public function getModel()
  {
    return $this->model;
  }

   /**
   * Setter de $data
   *
   * @param array $data
   */
  public function setData($data)
  {
    $this->data = $data;
  }

  /**
   * Getter $data
   *
   * @return array
   */
  public function getData()
  {
    return $this->data;
  }

   /**
   * Setter de $params
   *
   * @param array $params
   */
  public function setParams($params)
  {
    $this->params = $params;
  }

  /**
   * Getter $params
   *
   * @return array
   */
  public function getParams()
  {
    return $this->params;
  }
}
