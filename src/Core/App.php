<?php

namespace Plantera\Core;

use Exception;
use Plantera\Core\ClientPipe\Request;

/**
 * Class App
 * @package Plantera\Core
 *
 * Se encarga del funcionamiento y la ejecición de la aplicación
 */
class App
{
  /**
   * Ruta del ROOT
   *
   * @var string $rootPath
   */
  private static $rootPath;

  /**
   * Ruta de la carpeta app
   *
   * @var string $appPath
   */
  private static $appPath;

  /**
   * Ruta de la carpata public
   *
   * @var string $publicPath
   */
  private static $publicPath;

  /**
   * Ruta de la carpeta views
   *
   * @var string $viewsPath
   */
  private static $viewsPath;

  /**
   * Ruta actual
   *
   * @var string $urlPath
   */
  private static $urlPath;

  /**
   * Request ejecutada por el usuario
   *
   * @var Request
   */
  protected $request;

  /**
   * Controller que se va a ejecutar.
   *
   * @var mixed
   */
  protected $controller;

  /**
   * Constructor de la aplicación
   *
   * @param string $rootPath
   */
  public function __construct($rootPath)
  {
    self::$rootPath = $rootPath;
    self::$appPath = $rootPath . '/app';
    self::$publicPath = $rootPath . '/public';
    self::$viewsPath = $rootPath . '/views';
    self::$urlPath = "$_SERVER[REQUEST_SCHEME]://$_SERVER[SERVER_NAME]$_SERVER[REQUEST_URI]";
  }

  /**
   * Hace la ejecución de la aplicación
   *
   * @throws Exception
   */
  public function run()
  {
    $this->request = new Request();

    $methodRequest = $this->request->getRequestAction();
    $urlRequest = $this->request->getRequestUrl();

    if(Router::exists($methodRequest, $urlRequest)) {
      $controller = Router::getController($methodRequest, $urlRequest);
      $this->runController($controller['controller'], $controller['action']);
    }
    else {
      throw new Exception('No existe la ruta especificada.');
    }
  }

  /**
   * Se encarga de crear una instancia del controlador requerido y ejecutar el
   * método correspondiente.
   *
   * @param string $name
   * @param string $action
   */
  public function runController($name, $action)
  {
    try {
      $controller = "\\Plantera\\Controllers\\" . $name;
      $this->controller = new $controller($action);
    }
    catch(Exception $e) {
      View::render(5, $e->getMessage());
    }
  }

  // Encapsulamiento

  /**
   * Devuelve el valor de $rootPath
   *
   * @return string
   */
  public static function getRootPath()
  {
    return self::$rootPath;
  }

  /**
   * Devuelve el valor de $appPath
   *
   * @return string
   */
  public static function getAppPath()
  {
    return self::$appPath;
  }

  /**
   * Devuelve el valor de $publicPath
   *
   * @return string
   */
  public static function getPublicPath()
  {
    return self::$publicPath;
  }

  /**
   * Devuelve el valor de $viewsPath
   *
   * @return string
   */
  public static function getViewsPath()
  {
    return self::$viewsPath;
  }

  /**
   * Devuelve el valor de $urlPath
   *
   * @return string
   */
  public static function getUrlPath()
  {
    return self::$urlPath;
  }

}
