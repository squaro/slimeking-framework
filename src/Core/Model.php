<?php

namespace Plantera\Core;

use PDO;
use Exception;
use Plantera\Database\MySQL\DB;

/**
 * Class Model
 * @package Plantera\Core
 *
 * Clase generica que va a ser implementadas en los modelos de las tablas de la
 * base de datos.
 */
class Model
{
  /**
   * Nombre de la tabla
   *
   * @var string
   */
  protected $table = '';

  /**
   * Clave primaria de la tabla
   *
   * @var string|array
   */
  protected $primaryKey;

  /**
   * Lista de atributos de la tabla
   *
   * @var array
   */
  protected $attributes = [];

  /**
   * Constructor general de los modelos
   *
   * @param int|null
   */
  public function __construct($id = null)
  {
    if(!is_null($id)) {
      $this->getByPrimaryKey($id);
    }
  }

  /**
   * Metodo mágico __get()
   *
   * @param string $key
   * @return mixed
   */
  public function __get($key)
  {
    return $this->{$key};
  }

  /**
   * Metodo mágico __set()
   *
   * @param string $key
   */
  public function __set($key, $value)
  {
    $this->{$key} = $value;
  }

  /**
   * Index method
   *
   * @return array
   * @throws Exception
   */
  public function index()
  {
    $db = DB::getConnection();
    $query = "SELECT * FROM " . $this->table;

    $stmt = $db->prepare($query);
    $status = $stmt->execute();

    if(!$status) {
      throw new Exception("No se pudieron obtener los datos de $this->table");
    }

    $res = [];

    while($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $instance = new static;
      $instance->setDataFromRow($row);
      $res[] = $instance;
    }

    return $res;
  }

  /**
   * View method
   *
   * @param int|null $id
   * @throws Exception
   * @return mixed
   */
  public function view($id = null)
  {
    if(!$id) {
      throw new Exception('No se pudo obtener id de la instancia.');
    }

    return $this->getByPrimaryKey($id);
  }

  /**
   * Delete method
   *
   * @param int|array|null $data
   * @throws Exception
   */
  public function delete($data = null)
  {
    if(!$data) {
      throw new Exception('No se pudo obtener la informació de la instancia.');
    }

    $db = DB::getConnection();
    $query = $this->prepareDeleteQuery($data);
    $deleteData = $this->filterData($data);

    $stmt = $db->prepare($query);
    $status = $stmt->execute($deleteData);

    if(!$status) {
      throw new Exception('Ocurrio un error al borrar un registro.');
    }

    return compact('status');
  }

  /**
   * Add method
   *
   * @param array $data
   * @throws Exception
   * @return static
   */
  public function add($data)
  {
    $db = DB::getConnection();
    $query = $this->prepareInsertQuery($data);
    $insertData = $this->filterData($data);

    $stmt = $db->prepare($query);
    $status = $stmt->execute($insertData);

    if(!$status) {
      throw new Exception('No se pudo insertar la información en la base de datos.');
    }

    $instance = new static($db->lastInsertId());

    return $instance;
  }

  /**
   * Edit method
   *
   * @param array $data
   * @throws Exception
   * @return static
   */
  public function edit($data)
  {
    $db = DB::getConnection();
    $query = $this->prepareUpdateQuery($data);
    $insertData = $this->filterData($data);

    $stmt = $db->prepare($query);
    $status = $stmt->execute($insertData);

    if(!$status) {
      throw new Exception('No se pudo modificar la información en la base de datos.');
    }

    $instance = new static($data['id']);

    return $instance;
  }

  /**
   * Obtiene un registro por la PK
   *
   * @param int|array $primaryKey
   * @throws Exception
   * @return mixed|null
   */
  public function getByPrimaryKey($primaryKey)
  {
    $db = DB::getConnection();
    $query = $this->prepareQueryPrimaryKey($primaryKey);

    $stmt = $db->prepare($query);

    if(!is_array($primaryKey)) {
      $primaryKey = [$primaryKey];
    }

    $status = $stmt->execute($primaryKey);

    if(!$status) {
      throw new Exception('Error al ejecutar la consulta');
    }

    if($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
      $this->setDataFromRow($row);
    }
    else {
      return null;
    }
  }

  /**
   * Prepara la query para obtener registros por multiples claves primarias.
   *
   * @param string|array $primaryKey
   * @throws Exception
   * @return string
   */
  public function prepareQueryPrimaryKey($primaryKey)
  {
    $query = "SELECT * FROM $this->table WHERE ";

    if(is_array($primaryKey)) {
      if(count($this->primaryKey) == count($primaryKey)) {
        for($i = 0; $i < count($this->primaryKey); $i++) {
          $query .= ($i == 0) ? " " : " AND ";
          $query .= $this->primaryKey[$i] . ' = ?';
        }
      }
      else {
        throw new Exception('Error al obtener las claves primarias.');
      }
    }
    else {
      $query .= "$this->primaryKey = ?";
    }

    return $query;
  }

  /**
   * Genera las queries para insertar datos en la base
   *
   * @param array $data
   * @return string
   */
  public function prepareInsertQuery($data)
  {
    $query = "INSERT INTO $this->table (";
    $queryValues = " VALUES (";

    $fields = [];
    $holders = [];

    foreach($data as $key => $value) {
      $fields[] = $key;
      $holders[] = ':' . $key;
    }

    $query .= implode(',', $fields) . ')';
    $query .= $queryValues . implode(',', $holders) . ')';

    return $query;
  }

  /**
   * Genera las queries para borrar datos de la base.
   *
   * @param  int|array $data
   * @return string
   */
  public function prepareDeleteQuery($data)
  {
    $query = "DELETE FROM $this->table WHERE ";
    $fields = [];

    if(!is_array($data)) {
      $id = $data;
      $data = ['id' => $id];
    }

    foreach($data as $key => $value) {
      $fields[] = "$key = :$key";
    }

    $query .= implode(' AND ', $fields);

    return $query;
  }

  /**
   * Genera las queries para modificar datos en la base
   *
   * @param array $data
   * @return string
   */
  public function prepareUpdateQuery($data)
  {
    $query = "UPDATE $this->table SET";
    $queryValues = [];
    $whereQuery = " WHERE id = :id";

    $fields = [];
    $holders = [];

    foreach($data as $key => $value) {
      if($key != 'id') {
        $queryValues[] = " $key = :$key";
      }
    }

    $query .= implode(',', $queryValues);
    $query .= $whereQuery;

    return $query;
  }

  /**
   * Filtra las información correcta correspondiente a los atributos del modelo.
   *
   * @param array $data
   * @return array
   */
  public function filterData($data)
  {
    $res = [];

    if(!is_array($data)) {
      $id = $data;
      $data = ['id' => $id];
    }

    foreach($data as $key => $value) {
      if(in_array($key, $this->attributes)) {
        $res[$key] = $value;
      }
    }

    return $res;
  }

  /**
	 * Carga los datos del objeto en base al array proporcionado.
	 *
	 * @param array $data
	 */
	public function setDataFromRow($data)
	{
		foreach($this->attributes as $attr) {
			if(isset($data[$attr])) {
				$this->{$attr} = $data[$attr];
			}
		}
	}

  // Encapsulamiento

  /**
   * Hace el get de $table
   *
   * @return string
   */
  public function getTable()
  {
    return $this->table;
  }

  /**
   * Hace el set de $table
   *
   * @param string $table
   */
  public function setTable($table)
  {
    $this->table = $table;
  }

  /**
   * Hace el get de $primaryKey
   *
   * @return string
   */
  public function getPrimaryKey()
  {
    return $this->primaryKey;
  }

  /**
   * Hace el set de $primaryKey
   *
   * @param string $primaryKey
   */
  public function setPrimaryKey($primaryKey)
  {
    $this->primaryKey = $primaryKey;
  }

  /**
   * Hace el get de $attributes
   *
   * @return string
   */
  public function getAttributes()
  {
    return $this->attributes;
  }

  /**
   * Hace el set de $attributes
   *
   * @param string $attributes
   */
  public function setAttributes($attributes)
  {
    $this->attributes = $attributes;
  }
}
