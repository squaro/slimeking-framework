<?php

namespace Plantera\Security;

use Exception;
use Plantera\Security\Hash;

/**
 * Class Auth
 * @package Plantera\Security
 *
 * Contiene todas las funciones que sean respecto a la autenticación de los
 * usuarios.
 */
class Auth {

  /**
   * Valida el email y la contraseña de un usuario.
   *
   * @param array $data
   * @return User|null
   */
  public static function validateLogin($data)
  {
    $usersModel = new User;

    $user = $usersModel->getUserByEmail($data['email']);

    if(is_null($user)) {
      throw new Exception('Usuario y/o contraseña inválidos.');
    }

    if(Hash::verify($data['password'], $user->getPassword())) {
      return $user;
    }
    else {
      throw new Exception('Usuario y/o contraseña inválidos.');
    }
  }

  /**
   * Agrega controllers y methods al $authRules['deny']
   *
   * @param string $controller
   * @param array|null $methods
   */
  public static function deny($controller, $methods = null)
  {
    if(!is_null($methods)) {
      self::$authRules['deny'][$controller] =  $methods;
    }
    else {
      $controllerNamespace = "\\Plantera\\Controllers\\" . $controller;
      self::$authRules['deny'][$controller] = get_class_methods($controllerNamespace);
    }
  }

  /**
   * Valida si se recibio el token y si el metodo require autenticación.
   *
   * @param string $controller
   * @param string $method
   * @throws Exception
   */
  public static function validateAuth($controller, $method)
  {
    $controller = str_replace('Plantera\Controllers\\', '', $controller);

    if(!isset(self::$authRules['deny'][$controller])) {
      throw new Exception('No se definió el controlador en las restricciones.');
    }

    if(in_array($method, self::$authRules['deny'][$controller])) {

      $token = $_SERVER['HTTP_AUTHORIZATION'] ?? null;

      if($token == null || !Token::verify(str_replace('Bearer ', '', $token))) {
        throw new Exception('Necesitas estar autenticado para realizar esta acción.');
      }
    }
  }

  /**
   * Getter $authRules
   *
   * @return array
   */
  public static function getAuthRules()
  {
    return self::$authRules;
  }

}
