<?php

namespace Plantera\Security;

/**
 * Class Hash
 * @package Plantera\Security
 *
 * Se encarga de hacer el hasheo de las passwords.
 */
class Hash {
  /**
   * Hace el hash de la contraseña.
   *
   * @param String $password
   * @return String
   */
  public static function do($password)
  {
    return password_hash($password, PASSWORD_DEFAULT);
  }

  /**
   * Verifica si el hash y el password corresponden.
   *
   * @param String $password
   * @param String $hash
   * @return Bool
   */
  public static function verify($password, $hash)
  {
    return password_verify($password, $hash);
  }
}
