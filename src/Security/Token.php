<?php

namespace Plantera\Security;

use Exception;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Hmac\Sha256;

/**
 * Class Token
 * @package Plantera\Security
 *
 * Se encarga de el manejo de las funciones de Token
 */
class Token
{
  /**
   * Key de encriptación
   *
   * @var string
   */
  protected static $key = 'vT1oVwOAjqK1xPgzHSrnvYqE0dw2rRCu';

  /**
   * Emisor del token
   *
   * @var string
   */
  protected static $issuer = 'http://davinci.jgasparro.com.ar/';

  /**
   * Genera un token
   *
   * @param string $id
   * @return string
   */
  public static function generate($id)
  {
    $algorithm = new Sha256;
    $builder = new Builder;

    $builder->setIssuer(self::$issuer);
    $builder->setIssuedAt(time());
    $builder->set('id', $id);
    $builder->sign($algorithm, self::$key);

    $token = $builder->getToken();

    return (string) $token;
  }

  /**
   * Verifica si el token es válido
   *
   * @param string $token
   * @return array|boolean
   */
  public static function verify($token)
  {
    try {
      $algorithm = new Sha256;
      $parser = new Parser;
      $token = $parser->parse($token);

      $validationData = new ValidationData;
      $validationData->setIssuer(self::$issuer);

      if(!$token->validate($validationData) || !$token->verify($algorithm, self::$key)) {
        throw new Exception('El token es inválido');
      }

      return [
        'id' => $token->getClaim('id')
      ];
    }
    catch(Exception $e) {
      return [
        'error' => 0,
        'message' => $e->getMessage()
      ];
    }
  }

}
