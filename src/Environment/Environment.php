<?php

namespace Plantera\Environment;

use Exception;
use Dotenv\Dotenv;
use Plantera\Contracts\EnvironmentContract;
use Plantera\Environment\Exceptions\InvalidRootDirException;
use Plantera\Environment\Exceptions\EnvironmentFileNotFoundException;

class Environment implements EnvironmentContract
{
    /**
     * @var Dotenv Environment instance
     */
    private $dotenv;

    /**
     * @var string Root dir.
     */
    protected $rootDir;

    /**
     * @var string App environment.
     */
    protected $environment;

    /**
     * @var string Secret key app.
     */
    protected $secretKey;

    public function __construct($rootDir = null)
    {
        if(is_null($rootDir)) {

            throw new InvalidRootDirException;

        }

        try {
            $this->dotenv = Dotenv::create($rootDir);
            $this->dotenv->load();
            $this->rootDir = $rootDir;
            $this->environment = $this->get('APP_ENV');
            $this->secretKey = $this->get('APP_KEY');
        }
        catch (Exception $e) {
            throw new EnvironmentFileNotFoundException;
        }
    }

    /**
     * @return mixed
     */
    public function get($key)
    {
        return getenv($key);
    }

    /**
     * @param $key
     * @param $value
     */
    public function set($key, $value)
    {
        putenv("$key=$value");
    }

}