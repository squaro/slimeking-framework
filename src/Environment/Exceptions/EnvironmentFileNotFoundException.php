<?php

namespace Plantera\Environment\Exceptions;

use Exception;

class EnvironmentFileNotFoundException extends Exception{}