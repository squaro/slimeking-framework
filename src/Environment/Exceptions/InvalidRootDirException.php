<?php

namespace Plantera\Environment\Exceptions;

use Exception;

class InvalidRootDirException extends Exception{}