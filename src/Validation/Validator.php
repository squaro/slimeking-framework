<?php

namespace Plantera\Validation;

use Plantera\Validation\Exceptions\InvalidArgumentException;
use Plantera\Validation\Exceptions\InvalidRuleException;

/**
 * Class Validator
 * @package Plantera\Validation
 *
 * Se encarga de realizar las validaciones de los datos recibidos.
 */
class Validator {

    protected $data;
    protected $rules;
    protected $errors = [];

    /**
     * Constructor del validator
     *
     * @param array $data
     * @param array $rules
     * @throws \Plantera\Validation\Exceptions\InvalidArgumentException
     */
    public function __construct($data = null, $rules = null)
    {
        if(!is_array($data) || !is_array($rules)) {
            throw new InvalidArgumentException;
        }

        $this->data = $data;
        $this->rules = $rules;
    }

    /**
     * Hace las validaciones
     *
     * @return Validator
     */
    public function validate()
    {
        foreach($this->rules as $name => $settings) {

            $rules = explode('|', $settings);

            for($i = 0; $i < count($rules); $i++) {
                $this->executeRuleValidation($name, $rules[$i]);
            }
        }

        return $this;
    }

    /**
     * Retorna el estado de la validación de los campos.
     *
     * @return bool
     */
    public function isValid()
    {
        return count($this->errors) == 0 ? true : false;
    }

    /**
     * Ejecuta las validaciones.
     * En esta funcion utilizo una referencia que usa Phyton para manejar argumentos
     * que la llaman kwargs.
     *
     * @param string $name
     * @param string $rule
     * @throws InvalidRuleException;
     */
    public function executeRuleValidation($name, $rule)
    {
        if(strpos($rule, '=') === false) {
            $method = "_$rule";

            if(!method_exists($this, $method)) {
                throw new InvalidRuleException;
            }

            $this->{$method}($name);
        }
        else {
            $kwarg = explode('=', $rule);
            $method = '_' . $kwarg[0];
            $param = $kwarg[1];

            if(!method_exists($this, $method)) {
                throw new InvalidRuleException;
            }

            $this->{$method}($name, $param);
        }
    }

    /**
     * Agrega un error.
     *
     * @param string $name
     * @param string $message
     */
    public function addError($name, $message)
    {
        if(!isset($this->errors[$name])) {
            $this->errors[$name] = [];
        }

        array_push($this->errors[$name], $message);
    }

    /**
     * Obtiene todos los errores del validador.
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Verifica si el campo esta vacio.
     *
     * @param string $fieldName
     */
    public function _required($fieldName)
    {
        $value = $this->data[$fieldName];

        if($value == '') {
            $this->addError($fieldName, "El campo $fieldName no puede estar vacío.");
        }
    }

    /**
     * Verifica si el campo es numérico.
     *
     * @param string $fieldName
     */
    public function _numeric($fieldName)
    {
        $valueField = $this->data[$fieldName];

        if(!is_numeric($valueField)) {
            $this->addError($fieldName, "El campo $fieldName debe contener solo números.");
        }
    }

    /**
     * Verifica si el campo es válido como email.
     *
     * @param string $fieldName
     */
    public function _email($fieldName)
    {
        $valueField = $this->data[$fieldName];

        if(!filter_var($valueField, FILTER_VALIDATE_EMAIL)) {
            $this->addError($fieldName, "El campo $fieldName no es válido.");
        }
    }

    /**
     * Verifica si el campo se exede del maximo de los caracteres permitidos.
     *
     * @param string $fieldName
     * @param string $valueRule
     */
    public function _max_length($fieldName, $valueRule)
    {
    $valueField = $this->data[$fieldName];

        if(strlen($valueField) > $valueRule) {
            $this->addError($fieldName, "El campo $fieldName se excede del máximo de caracteres permitidos (Máx. $valueRule).");
        }
    }

    /**
     * Verifica si el campo esta por debajo de los caracteres mínimos establecidos.
     *
     * @param string $fieldName
     * @param string $valueRule
     */
    public function _min_length($fieldName, $valueRule)
    {
        $valueField = $this->data[$fieldName];

        if(strlen($valueField) < $valueRule) {
            $this->addError($fieldName, "El campo $fieldName está por debajo del mínimo de caracteres permitidos (Mín. $valueRule).");
        }
    }

}
