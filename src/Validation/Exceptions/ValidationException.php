<?php

namespace Plantera\Validation\Exceptions;

use Exception;

class ValidationException extends Exception {}
