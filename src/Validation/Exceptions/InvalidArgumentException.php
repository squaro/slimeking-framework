<?php

namespace Plantera\Validation\Exceptions;

use Exception;

class InvalidArgumentException extends Exception {}
