<?php

namespace Plantera\Validation\Exceptions;

use Exception;

class InvalidRuleException extends Exception {}
