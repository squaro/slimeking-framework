<?php

namespace Plantera\Contracts;

interface EnvironmentContract
{
    /**
     * @param $key
     * @return mixed
     */
    public function get($key);

    /**
     * @param $key
     * @param $value
     * @return void
     */
    public function set($key, $value);
}