<?php

  namespace Plantera\Storage;

  class Session {

    /**
     * Inicia el uso de las sesiones.
     */
    public static function start() {
      session_start();
    }

    /**
     * Finaliza el uso de las sesiones.
     */
    public static function destroy() {
      session_destroy();
    }

    /**
     * Agrega una variable de session.
     *
     * @param String $key
     * @param Any $value
     */
    public static function add($key, $value) {

      $_SESSION[$key] = $value;

    }

    /**
     * Obtiene una variable de sesion.
     *
     * @param String $key
     * @return Any
     */
    public static function get($key) {

      return isset($_SESSION[$key]) ? $_SESSION[$key] : null;

    }

    /**
     * Obtiene una variable de sesion y la elimina.
     *
     * @param String $key
     * @return Any
     */
    public static function once($key) {

      $value = $_SESSION[$key];
      self::remove($key);
      return $value;

    }

    /**
     * Elimina una variable de sesion.
     *
     * @param String $key
     */
    public static function remove($key) {

      unset($_SESSION[$key]);

    }

    /**
     * Retorna si existe el estado de una variable de sesion.
     *
     * @param String $key
     * @return Bool
     */
    public static function exists($key) {

      return isset($_SESSION[$key]);

    }

  }
