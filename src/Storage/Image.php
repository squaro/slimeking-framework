<?php

namespace Plantera\Storage;

use Exception;
use Plantera\Core\App;

/**
 * Class Image
 * @package Plantera\Storage
 *
 * Se encarga de hacer el parseo del base64 para que se transforme a un binario
 * y asi poder almacenarla en el sistema.
 */
class Image
{

  /**
   * Ruta donde se van a guardar las imágenes.
   *
   * @var string
   */
  protected $imagePath;

  /**
   * Imagen binaria
   *
   * @var image
   */
  protected $imageSource;

  // protected $route;

  /**
   * Contructor
   *
   * @param string $base64
   */
  public function __construct($base64) {
    $this->imagePath = '/images/';

    $this->convertFromBase64($base64);
  }

  /**
   * Metodo mágico __get()
   *
   * @param string $key
   * @return mixed
   */
  public function __get($key)
  {
    return $this->{$key};
  }

  /**
   * Metodo mágico __set()
   *
   * @param string $key
   */
  public function __set($key, $value)
  {
    $this->{$key} = $value;
  }

  /**
   * Transforma la imagen de base64 a binario.
   *
   * @param string $base64
   */
  public function convertFromBase64($base64)
  {
    $cleanBase64 = str_replace('data:image/jpeg;base64,', '', $base64);

    $decodedBase64 = base64_decode($cleanBase64);

    $this->imageSource = imagecreatefromstring($decodedBase64);
  }

  /**
   * Hace la subida de imagenes al servidor
   *
   * @return string
   * @throws Exception
   */
  public function upload() {

    if(isset($this->imageSource)) {

      $imageName = time() . '.jpg';
      $imageRoute = App::getPublicPath() . $this->imagePath . $imageName;

      imagejpeg($this->imageSource, $imageRoute);

      return $imageName;

    }
    else {
      throw new Exception('No se ha definido la imagen a subir.');
    }

  }

  /**
   * Setter $imageSource
   *
   * @param String $imageSource
   */
  public function setImageSource($imageSource) {

    $this->imageSource = $imageSource;

  }

  /**
   * Getter $imageSource
   *
   * @return String
   */
  public function getImageSource() {

    return $this->imageSource;

  }

  /**
   * Setter $imagePath
   *
   * @param String $imagePath
   */
  public function setImagePath($imagePath) {

    $this->imagePath = $imagePath;

  }

  /**
   * Getter $imagePath
   *
   * @return String
   */
  public function getImagePath() {

    return $this->imagePath;

  }

}
