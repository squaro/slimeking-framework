<?php

namespace Plantera\Database\MySQL;

use PDO;
use Exception;

class DB {

	private static $db;

	/**
	 * Método privado - Singleton
	 */
	private function __construct() {}

	/**
	 * Método publico para obtener la conexión
	 *
	 * @return PDO
	 */
	public static function getConnection()
	{
		if(!self::$db) {
			$host = 'localhost';
			$user = 'root';
			$password = '';
			$database = 'dwtn4a_programacion_iii_final';
			$dsn = "mysql:host=$host;dbname=$database;charset=utf8";

			try {
				self::$db = new PDO($dsn, $user, $password);
			}
			catch(Exception $e) {
				throw new Exception('Error al intentar conectar con MySQL.');
			}
		}
		return self::$db;
	}
}
